package com.rpgfarm.crongsub;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.rpgfarm.crongsub.main;

import ch.njol.skript.Skript;
import com.rpgfarm.crongsub.SSocket;

public class main
  extends JavaPlugin implements Listener
{
  public static FileConfiguration config;
  public Thread serverThread;
  public static String latestCommand = "";
  String ip;
  
  public static String m(String message)
  {
    return ChatColor.translateAlternateColorCodes('&', message);
  }
	  public void onEnable()
	  {

	    config = getConfig();
	    config.addDefault("setting.coreport", Integer.valueOf(3208));
	    config.addDefault("setting.onlinecmd", "say 어떤 서버에서 CrongSub가 활성화되었습니다.");
	    config.addDefault("setting.offlinecmd", "say 어떤 서버에서 CrongSub가 비활성화되었습니다.");
	    config.options().copyDefaults(true);
	    saveConfig();
	    saveDefaultConfig();
	    Bukkit.getConsoleSender().sendMessage("Crong Sub가 로드중입니다. Powered by Baw Service");
	    Bukkit.getConsoleSender().sendMessage("해당 플러그인은 크롱 클라우드에서 구동되는 크롱 서버에게만 사용이 허가됩니다.");
	    Bukkit.getConsoleSender().sendMessage("Crong Core로 서버 상태 보고중입니다.");
	    ClientRun("command;" + config.getString("setting.onlinecmd"));
	    getServer().getPluginManager().registerEvents(this, this);
	    Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
	    Bukkit.getConsoleSender().sendMessage("BungeeCord 서비스를 등록합니다.");

	    Bukkit.getConsoleSender().sendMessage("Skript 서비스를 등록합니다.");
	    Skript.registerAddon(this);
		Skript.registerEffect(SSocket.class, "SendSocket %string%");
	    
	  }
	  
	  public void onDisable()
	  {
		Bukkit.getConsoleSender().sendMessage("Crong Sub가 비할성화중입니다. Powered by Baw Service");
	    Bukkit.getConsoleSender().sendMessage("Crong Core로 서버 상태 보고중입니다.");
	    ClientRun("command;" + config.getString("setting.offlinecmd"));
	  }
	  
	  
	public static void ClientRun(String data){
	    
	    Socket socket = null;
	    OutputStream os = null;
	    OutputStreamWriter osw = null;
	    BufferedWriter bw = null;
	    
	    InputStream is = null;
	    InputStreamReader isr = null;
	    BufferedReader br = null;
	    
	    try{
	        socket = new Socket("localhost", config.getInt("setting.coreport"));
	        os = socket.getOutputStream();
	        osw = new OutputStreamWriter(os);
	        bw = new BufferedWriter(osw);
	    
	        is = socket.getInputStream();
	        isr = new InputStreamReader(is, "UTF-8");
	        br = new BufferedReader(isr);
	        
	        bw.write(data);
	        bw.newLine();
	        bw.flush();
	        
	        /*String receiveData = "";
	        receiveData = br.readLine();*/
	    }catch(Exception e){
	        e.printStackTrace();
	    }finally {
	        try{
	            bw.close();
	            osw.close();
	            os.close();
	            br.close();
	            isr.close();
	            is.close();
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	    }    
	    
	}

  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("이 명령어는 콘솔에서 실행할 수 없습니다.");
      return false;
    }
    Player p = (Player)sender;
    if ((commandLabel.equalsIgnoreCase("CCWARP")) && 
      (args.length == 1))
    {
      ByteArrayDataOutput out = ByteStreams.newDataOutput();
      out.writeUTF("Connect");
      out.writeUTF(args[0].toString());
      p.sendPluginMessage(this, "BungeeCord", out.toByteArray());
      return true;
    }
    return true;
  }
}
